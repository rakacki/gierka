package com.sda.dp.game.view;

import com.sda.dp.game.dispatcher.Dispatcher;
import com.sda.dp.game.interfaces.IFireListener;
import com.sda.dp.game.model.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainPanel extends JPanel implements IFireListener {
    private final Color BACKGROUND_COLOR = Color.LIGHT_GRAY;
    private final int width;
    private final int height;
    private final int MAX_MONSTER_SHOTS = 2;

    private List<AbstractGameObject> objectList; //monstery
    private List<AbstractGameObject> FireList;
    private List<AbstractGameObject> monsterFireList;

    private Heart heart;
    private GameHero hero;
    private GameText gameOverTekst;

    public MainPanel(int width, int height) {
        super();
        this.width = width;
        this.height = height;

        heart = new Heart();
        gameOverTekst = new GameText(width/2, height/2,"GAME OVER YOU LOOSER!!!");

        // create stuff
        objectList = new ArrayList<>();
        FireList = new ArrayList<>();
        monsterFireList = new ArrayList<>();

        // set stuff
        setBorder(BorderFactory.createLineBorder(Color.black, 2));
        setLayout(null);

        Dispatcher.instance.registerObject(this);
    }

    public void addCharacterIntoGame(AbstractGameObject gameObject){
        objectList.add(gameObject);
    }

    public void addFireIntoGame(AbstractGameObject gameObject){
        FireList.add(gameObject);
    }

    private void addMonsterFireIntoGame(Fire fire) {
        monsterFireList.add(fire);
    }

    public void setGameHero(GameHero hero){
        this.hero = hero;
    }

    // RYSOWANIE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;

        //rysowanie tła
        g2d.setColor(BACKGROUND_COLOR);
        g2d.fillRect(0, 0, width, height);
        paintWorld(g2d);

        if(hero.getheathPoints()>0){
            paintWorld(g2d);
        }else{
            paintGameOver(g2d);
        }

    }

    private void paintGameOver(Graphics2D g2d) {
        gameOverTekst.paint(g2d);
    }

    private void paintWorld(Graphics2D g2d) {
        //rysowanie serduszek
        for(int i=0; i < hero.getheathPoints(); i++){
            heart.paint(g2d, i);
        }

        g2d.setColor(Color.white);

        //rysowanie postaci
        AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
        objectsToPaint = objectList.toArray(objectsToPaint);

        for (AbstractGameObject objectToPaint : objectsToPaint) {
            objectToPaint.paint(g2d);
        }

        //rysowanie strzałów bohatera
        AbstractGameObject[] firesToPaint = new AbstractGameObject[FireList.size()];
        firesToPaint = FireList.toArray(firesToPaint);

        for (AbstractGameObject fireToPaint : firesToPaint) {
            fireToPaint.paint(g2d);
        }

        //rysowanie strzałów potworów
        AbstractGameObject[] monsterFires = new AbstractGameObject[monsterFireList.size()];
        monsterFires = monsterFireList.toArray(monsterFires);

        for (AbstractGameObject monsterFire : monsterFires) {
            monsterFire.paint(g2d);
        }


        hero.paint(g2d);
    }


    // RUCH !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public void move(double move){
        Random r = new Random();

        if(move > 0) {
            //alokacja tablicy o rozmiarze oryginalnej listy
            AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
            //kopiowanie obiektów
            objectsToPaint = objectList.toArray(objectsToPaint);
            //iterujemy tablice zamiast listy
            // czyli rozwiązujemy problem: nie usuwamy z kolekcji, którą iterujemy
            //iterujemy tablicę, usuwamy z listy
            for (AbstractGameObject objectToPaint : objectsToPaint) {
                if(objectToPaint != null) {
                    objectToPaint.move(move);
                }
                if(monsterFireList.size() < MAX_MONSTER_SHOTS){
                    // generuj strzał potwora z prawdopodobieństwem wystrzału
                    if(r.nextInt(1000) >= (1000 - objectList.size())){
                        addMonsterFire(objectToPaint.getPositionX(), objectToPaint.getPositionY());
                    }
                }
            }



            // ruch strzałów bohatera
            AbstractGameObject[] firesToMove = new AbstractGameObject[FireList.size()];
            firesToMove = FireList.toArray(firesToMove);
            for (AbstractGameObject fireToMove : firesToMove) {
                if(fireToMove != null) {
                    fireToMove.move(move);
                }
            }

            // ruch strzałów potworów
            AbstractGameObject[] monsterFiresToMove = new AbstractGameObject[monsterFireList.size()];
            monsterFiresToMove = monsterFireList.toArray(monsterFiresToMove);
            for (AbstractGameObject monsterFireToMove : monsterFiresToMove) {
                if(monsterFireToMove != null) {
                    monsterFireToMove.move(move);
                }
                if(monsterFireToMove.getPositionY() > height){
                    monsterFireList.remove(monsterFireToMove);
                }
            }

            hero.move(move);
        }
    }


    // KOLIZJE
    public void checkCollisions() {

        //my strzelamy w potwory
        AbstractGameObject[] objectsToMove = new AbstractGameObject[objectList.size()];
        objectsToMove = objectList.toArray(objectsToMove);
        AbstractGameObject[] firesToMove = new AbstractGameObject[FireList.size()];
        firesToMove = FireList.toArray(firesToMove);

        // ConcurrentModificationException

        for (AbstractGameObject strzal : firesToMove){
            if(!strzal.isToBeRemoved()){
                for (AbstractGameObject postac : objectsToMove) {
                    if (postac.checkCollisions(strzal) && !strzal.isToBeRemoved()) {
                     strzal.setToBeRemoved(true);
                     postac.hit();
                        if (postac.isToBeRemoved()) {
                            objectList.remove(postac);
                        }
                        FireList.remove(strzal);
                         // nie możemy usuwać
                        // usuwanie z iterowanej listy nie jest dozwolone
                     }
                }
            }
        }


        //potwory strzelają w nas
        AbstractGameObject[] fires = new AbstractGameObject[monsterFireList.size()];
        fires = monsterFireList.toArray(fires);

        for (AbstractGameObject strzalPotwora: fires) {
            if(hero.checkCollisions(strzalPotwora)){
                hero.hit();
                monsterFireList.remove(strzalPotwora);
            }
        }

    }



    @Override
    public void addFire(int posX, int posY, double speed, boolean up) {
        addFireIntoGame(new Fire(posX, posY, speed, up));
    }

    public void addMonsterFire(int posX, int posY) {
        addMonsterFireIntoGame(new Fire(posX, posY, 5, false));
    }


}
