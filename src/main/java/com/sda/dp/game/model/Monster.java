package com.sda.dp.game.model;

import com.sda.dp.game.HorizontalDirection;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Monster extends AbstractGameObject {

    private int hp; // wytrzymałosc na ciosy
    private BufferedImage image; //obrazek potwora

    private Point initialPosition;
    private int moveRange;

    public Monster(int x, int y, int durability, int range) {
        super(new Point(x, y));
        this.hp = durability;
        this.moveRange = range;
        this.initialPosition = new Point(x, y);
        try {
            image = ImageIO.read(new File("src/main/resources/mucha.png"));
        } catch (IOException ex) {
            System.out.println("Nie można załadować obrazka");
        }

        speed = 0.8;
        hDir= HorizontalDirection.RIGHT;
    }

    @Override
    public void move(double move) {
        // różnica pozycji - czyli o ile się przesunął
        int differenceX = position.x - initialPosition.x;
        // jeśli przekracza zakres naszego ruchu
        if (differenceX > moveRange){
            hDir = HorizontalDirection.LEFT;
        }else if(differenceX < 0){
            hDir = HorizontalDirection.RIGHT;
        }
        super.move(move);
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);

        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public void hit() {
        hp--;
        if(hp==0){
            toBeRemoved = true;
        }

    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

}
