package com.sda.dp.game.model;

import java.awt.*;

public class GameText extends AbstractGameObject {

    private String text;

    public GameText(int x, int y, String text) {
        super(new Point(x, y));
        this.text = text;
    }

    @Override
    public void paint(Graphics2D g2d) {
        Color currentColor = g2d.getColor();

        // set color
        g2d.setColor(Color.RED);
        // paint something
        g2d.drawString(text, position.x, position.y );

        // set last color
        g2d.setColor(currentColor);
    }

    @Override
    public void hit() {

    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int getWidth() {
        return 0;
    }
}
