package com.sda.dp.game.model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Heart extends AbstractGameObject {

    private BufferedImage image; //obrazek serduszka

    public Heart() {
        try {
            image = ImageIO.read(new File("src/main/resources/serce.png"));
        } catch (IOException ex) {
            System.out.println("Nie można załadować obrazka");
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public void hit() {

    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    public void paint(Graphics2D g2d, int i) {
        position.x = i * getWidth() + i * 5;
        position.y =0;

        paint(g2d);
    }
}
