package com.sda.dp.game.model;

import com.sda.dp.game.dispatcher.Dispatcher;
import com.sda.dp.game.dispatcher.HeroFiredEvent;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameHero extends AbstractGameObject {

    private BufferedImage image;
    private int heathPoints = 3;



    public GameHero(int x, int y) {
        super(new Point(x, y));
        try {
            image = ImageIO.read(new File("src/main/resources/zaba.png"));
        } catch (IOException ex) {
            // handle exception...
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public void hit() {
        heathPoints--;
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    public void fire(){
        // szerokość obrazka/2 + pozycja postaci - szerokość strzału
        int positionX = image.getWidth()/2 + position.x - 5;

        Dispatcher.instance.dispatch(new HeroFiredEvent(positionX, position.y));
    }

    public int getheathPoints() {
        return heathPoints;
    }
}
